from paramecio2.libraries.config_admin import config_admin
from paramecio2.libraries.i18n import I18n

#modules_admin=[[I18n.lang('admin', 'users_admin', 'User\'s Admin'), 'paramecio.modules.admin.admin.ausers', 'ausers']]

config_admin.append([I18n.lang('pastafari2', 'pastafari_admin', 'Pastafari admin')])

config_admin.append([I18n.lang('pastafari2', 'settings', 'Settings'), 'modules.pastafari2.admin.dashboard', 'admin_app.pastafari2_settings', 'fa-code'])
config_admin.append([I18n.lang('pastafari2', 'servers', 'Servers'), 'modules.pastafari2.admin.dashboard', 'admin_app.pastafari2_dashboard', 'fa-linux'])
config_admin.append([I18n.lang('pastafari2', 'groups', 'Groups'), 'modules.pastafari2.admin.groups', 'admin_app.pastafari2_groups', 'fa-object-group'])
#config_admin.append([I18n.lang('pastafari2', 'tasks_edit', 'Tasks edit'), 'modules.pastafari2.admin.tasks_edit', 'admin_app.pastafari2_tasks_edit', 'fa-tasks'])
config_admin.append([I18n.lang('pastafari2', 'tasks_log', 'Tasks log'), 'modules.pastafari2.admin.tasks', 'admin_app.pastafari2_tasks', 'fa-file-text-o'])
