#!/usr/bin/env python3

import os
from collections import OrderedDict

def scandir(mydir, config_parser, arr_dir=OrderedDict(), father=''):
    
    search_dir=os.listdir(mydir)
    
    for one_path in search_dir:
       
        if os.path.isfile(mydir+'/'+one_path):
            if one_path=='info.cfg':
                # Read the info file and put radio form in python files signed in file
                config_parser.clear()
                config_parser.read(mydir+'/'+one_path)
                
                if 'info' in config_parser:                   
                    if 'name' in config_parser['info']:
                        
                        if not father in arr_dir:
                            arr_dir[father]={}
                        
                        arr_dir[father][os.path.basename(mydir)]=[]
                        
                        arr_dir[father][os.path.basename(mydir)].append([config_parser['info']['name'], father, 0])
                        
                        if 'modules' in config_parser:
                            for k, v in config_parser['modules'].items():
                                #arr_dir[father][os.path.basename(mydir)].append([config_parser['modules'][k], s.dumps({'file': mydir+'/'+k+'.py'}), 1])
                                form=0
                                
                                if 'form' in config_parser:
                                    if k in config_parser['form']:
                                        form=config_parser['form'][k]
                                
                                arr_dir[father][os.path.basename(mydir)].append([config_parser['modules'][k], mydir+'/'+k+'.py', 1, form])
                
        
        elif os.path.isdir(mydir+'/'+one_path):
            
            arr_dir=scandir(mydir+'/'+one_path, config_parser, arr_dir, os.path.basename(mydir))
    
    return arr_dir

