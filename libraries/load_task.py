from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.db import corefields
from paramecio2.libraries.db.extrafields.ipfield import IpField
from paramecio2.libraries.db.extrafields.arrayfield import ArrayField
from paramecio2.libraries.db.extrafields.parentfield import ParentField
from paramecio2.libraries.db.extrafields.datefield import DateField
from paramecio2.libraries.db.extrafields.dictfield import DictField
from paramecio2.libraries.db.extrafields.jsonfield import JsonValueField
from paramecio2.libraries.i18n import I18n
import urllib3
try:
    import ujson as json
except:
    import json

from redis import Redis
from rq import Queue
import importlib
import traceback
from modules.pastafari2.models.tasks import Task

def load_db_in_task(task_id, conn):
    
    task_model=Task(conn)
    
    arr_task=task_model.select_a_row(task_id)
     
    if not arr_task:
        return False
    
    ssh_task=importlib.import_module(arr_task['path'])
    
    server=arr_task['server']
    remote_user=arr_task['user']
    remote_password=arr_task['password']
    private_key=arr_task['ssh_key_priv']
    password_key=arr_task.get('ssh_key_password', '')
    
    final_task=ssh_task.ServerTask(server, conn, remote_user=remote_user, remote_password=remote_password, private_key=private_key, password_key=password_key, remote_path='pastafari2', task_id=task_id, data=json.loads(arr_task['data']))
    
    return final_task
