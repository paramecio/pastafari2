from modules.pastafari2.models.tasks import Task, LogTask
from flask import request
from paramecio2.libraries.i18n import I18n
from modules.pastafari2.libraries.load_task import load_db_in_task

def load_progress(db, t, return_tree='', path_module='admin_app.pastafari2_dashboard'):

    task_id=request.args.get('task_id', '0')
    
    ssh_task=load_db_in_task(int(task_id), db)
    
    path_module='admin_app.pastafari2_dashboard'
    
    if ssh_task:
        if return_tree=='':
            return_tree=ssh_task.links
        if path_module=='admin_app.pastafari2_dashboard':
            path_module=ssh_task.path_module
    
    
    position=request.args.get('position', '0')
    
    task=Task(db)
    
    arr_task=task.set_conditions('WHERE id=%s', [task_id]).select_a_row_where()
    
    #print(url_return)
    
    #print(arr_task)
    if arr_task:
        
        url_return=arr_task['url_return']
        
        return t.load_template('progress.phtml', title=I18n.lang('pastafari2', 'task_progress', 'Task progress'), path_module=path_module, name_task=arr_task['name_task'], description_task=arr_task['description_task'], position=position, task_id=task_id, server=arr_task['server'], hostname=arr_task['hostname'], url_return=url_return, return_tree=return_tree)
    else:
        
        return ""
