from settings import config
from flask import g, url_for, request, session, make_response
from paramecio2.libraries.generate_admin_class import GenerateAdminClass
from paramecio2.libraries.i18n import I18n
from paramecio2.modules.admin import admin_app, t as admin_t
from paramecio2.libraries.db.coreforms import SelectForm
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.libraries import datetime
from paramecio2.libraries.urls import make_media_url
from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.lists import AjaxList
from paramecio2.libraries.db.extraforms.fileform import FileForm
from paramecio2.libraries.formsutils import show_form
from modules.pastafari2.libraries.task import Task as SSHTask
from modules.pastafari2.libraries.configtask import config_task
from modules.pastafari2.models.tasks import Task, LogTask
from modules.pastafari2.models.pastafari2 import ServerGroup
    
@admin_app.route('/pastafari2/groups/', methods=['GET', 'POST'])
def pastafari2_groups():    

    t=admin_t

    db=g.connection

    groups=ServerGroup(db)
    
    url=url_for('admin_app.pastafari2_groups')
    
    admin=GenerateAdminClass(groups, url, t)
    
    admin.list.raw_query=False
    
    form_admin=admin.show()
    
    if type(form_admin).__name__=='str':
        
        return t.load_template('content.phtml', title=I18n.lang('admin', 'groups_edit', 'Groups edit'), contents=form_admin, path_module='admin_app.pastafari2_groups')    
    else:
        
        return form_admin    

    return ""
