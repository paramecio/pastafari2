from settings import config
from flask import g, url_for, request, session, make_response
from paramecio2.libraries.generate_admin_class import GenerateAdminClass
from paramecio2.libraries.i18n import I18n
from paramecio2.modules.admin import admin_app, t as admin_t
from paramecio2.libraries.db.coreforms import SelectForm
from paramecio2.libraries.mtemplates import PTemplate, env_theme
from paramecio2.libraries import datetime
from paramecio2.libraries.urls import make_media_url
from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.lists import AjaxList
from paramecio2.libraries.db.extraforms.fileform import FileForm
from paramecio2.libraries.formsutils import show_form
from modules.pastafari2.libraries.task import Task as SSHTask
from modules.pastafari2.libraries.configtask import config_task
#from modules.pastafari2.models.scripts import Task, LogTask
from modules.pastafari2.models.pastafari2 import NameServerScripts, ServerScripts
from modules.pastafari2.models.pastafari2 import ServerGroup
from paramecio2.libraries.lists import SimpleList
from paramecio2.libraries.db.coreforms import SelectForm, SelectModelForm, HiddenForm
    
@admin_app.route('/pastafari2/tasks_edit/', methods=['GET', 'POST'])
def pastafari2_tasks_edit():    

    t=admin_t

    db=g.connection

    scripts=NameServerScripts(db)
    
    url=url_for('admin_app.pastafari2_tasks_edit')
    
    admin=GenerateAdminClass(scripts, url, t)
    
    admin.list.raw_query=False
    
    #request.args['order']=request.args.get('order', '1')
    admin.list.order='1'
    
    admin.list.fields_showed=['id', 'name']
    
    admin.list.arr_extra_options=[scripts_options]
    
    admin.no_insert=True
        
    admin.no_delete=True
    
    form_admin=admin.show()
    
    if type(form_admin).__name__=='str':
        
        return t.load_template('content.phtml', title=I18n.lang('admin', 'tasks_edit', 'Tasks edit'), contents=form_admin, path_module='admin_app.pastafari2_tasks_edit')    
    else:
        
        return form_admin    

    return ""

def scripts_options(url, row_id, row):
    """
    if row['is_parent']:
    
        return ['<a target="_blank" href="'+url_for('admin_app.pastafari2_multiprogress', task_id=row_id)+'">'+I18n.lang('pastafari2', 'view_task_log', 'View task log')+'</a>']
        
    else:
        
        return ['<a target="_blank" href="'+url_for('admin_app.pastafari2_progress', task_id=row_id)+'">'+I18n.lang('pastafari2', 'view_task_log', 'View task log')+'</a>']
    """
    #return ['<a href="">'+I18n.lang('pastafari2', 'edit_scripts', 'Edit scripts')+'</a>']
    
    arr_options=SimpleList.standard_options(url, row_id, row)
    
    arr_options.append('<a href="'+url_for('admin_app.pastafari2_scripts_edit', scripts_id=row_id)+'">'+I18n.lang('pastafari2', 'edit_scripts', 'Edit scripts')+'</a>')
        
    return arr_options
    
    
@admin_app.route('/pastafari2/scripts_edit/', methods=['GET', 'POST'])
def pastafari2_scripts_edit():    
    """Simple function for edit and add scripts to a task"""

    t=admin_t

    db=g.connection
    
    scripts_id=request.args.get('scripts_id', '0')
    
    scripts=ServerScripts(db)
    
    scripts.fields['scripts_id'].name_form=HiddenForm

    scripts.fields['scripts_id'].extra_parameters=[]
    
    scripts.fields['scripts_id'].default_value=scripts_id
    
    scripts.enctype=True
    
    scripts.fields['file'].yes_prefix=False
    
    server=NameServerScripts(db)
    
    url=url_for('.pastafari2_scripts_edit', scripts_id=scripts_id)
    
    arr_server=server.select_a_row(scripts_id)
    
    if arr_server:
    
        scripts.set_conditions('WHERE scripts_id=%s', [scripts_id])
        
        admin=GenerateAdminClass(scripts, url, t)
        
        admin.list.fields_showed=['name', 'file', 'position']
        
        admin.list.yes_search=False
        
        #admin.list.table_div=True
        
        admin.list.order_field='position'
        
        form_admin=admin.show()
        
        if type(form_admin).__name__=='str':
        
            return t.load_template('edit_scripts.phtml', title=I18n.lang('pastafari2', 'edit_script', 'Edit scripts'), path_module='admin_app.pastafari2_edit_scripts', server_scripts=arr_server, edit_update=form_admin)
            
        else:
            return form_admin
    
    else:
        
        return ""
