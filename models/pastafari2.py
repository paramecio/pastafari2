from paramecio2.libraries.db.webmodel import WebModel
from paramecio2.libraries.db import corefields
from paramecio2.libraries.db.extrafields.dictfield import DictField
from paramecio2.libraries.db.extrafields.datefield import DateField
from paramecio2.libraries.db.extrafields.datetimefield import DateTimeField
from paramecio2.libraries.db.extrafields.ipfield import IpField
from paramecio2.libraries.db.extrafields.urlfield import UrlField
from paramecio2.libraries.db.extrafields.urlfield import DomainField
from paramecio2.libraries.db.extrafields.dictfield import DictField
from paramecio2.libraries.db.extrafields.jsonfield import JsonValueField
from paramecio2.libraries.db.extrafields.parentfield import ParentField
from paramecio2.libraries.db.extrafields.filefield import FileField
from paramecio2.libraries.urls import make_media_url
from paramecio2.libraries import datetime
from paramecio2.modules.admin.models.admin import UserAdmin
from modules.pastafari2.models.tasks import LonelyIpField
#from modules.monit.models.monit import Server

class ServerGroup(WebModel):
    
    def __init__(self, connection):
        
        super().__init__(connection)
        self.register(corefields.CharField('group'), True)
        self.register(corefields.CharField('code_group'), True)
        
class Server(WebModel):
    
    def __init__(self, connection=None):
        
        super().__init__(connection)
        
        #self.register(corefields.ForeignKeyField('user_id', UserServer(connection)), True)                
        self.register(DomainField('hostname'), True)
        self.register(corefields.CharField('group'), True)
        self.register(LonelyIpField('ip'), True)
        self.fields['ip'].unique=True
        #self.fields['ip'].indexed=True

class ServerDbTask(Server):
    
    def __init__(self, connection=None):
        
        super().__init__(connection)
        self.fields['group'].required=False
        self.register(corefields.ForeignKeyField('group_id', ServerGroup(connection), 11, False, 'id', 'group', select_fields=[]))
        self.register(corefields.IntegerField('ssh_port'), True)

class UpdateServerScripts(WebModel):
    
    def __init__(self, connection=None):
        
        super().__init__(connection)
        self.register(corefields.CharField('name'), True)
        self.register(FileField('file', './scripts/local/'), True)
        self.register(corefields.CharField('args'))
        self.register(corefields.ForeignKeyField('server_id', ServerDbTask(connection), 11, False, 'id', 'group', select_fields=[]))
        self.register(corefields.IntegerField('position'))
        self.register(corefields.CharField('code'))

class NameServerScripts(WebModel):
    
    def __init__(self, connection=None):
        
        super().__init__(connection)
        self.register(corefields.CharField('name'), True)

class ServerScripts(WebModel):
    
    def __init__(self, connection=None):
        
        super().__init__(connection)
        self.register(corefields.CharField('name'), True)
        self.register(FileField('file', './scripts/local/tasks'), True)
        self.register(corefields.ForeignKeyField('scripts_id', NameServerScripts(connection), 11, False, 'id', 'name', select_fields=[]))
        self.register(corefields.IntegerField('position'))
        self.register(corefields.CharField('question0'))
        self.register(corefields.CharField('question1'))
        self.register(corefields.CharField('question2'))
        self.register(corefields.CharField('question3'))
        self.register(corefields.CharField('question4'))
        self.register(corefields.CharField('question5'))
        self.register(corefields.CharField('question6'))
        self.register(corefields.CharField('question7'))
        self.register(corefields.CharField('question8'))
        self.register(corefields.CharField('question9'))
        
