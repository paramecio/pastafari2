#!/opt/pythonenv/bin/python3 -u

# A script for install pzoo user

import subprocess
import argparse
import re
import os
import shutil
import pwd
from subprocess import call
from pathlib import Path
from time import sleep
import distro

parser = argparse.ArgumentParser(description='A script for install leviathan user')

#parser.add_argument('--url', help='The url where notify updates', required=True)
parser.add_argument('--url_stats', help='The url where pastafaristats notify the stats', required=True)
parser.add_argument('--user', help='The user for pastafari', required=True)
parser.add_argument('--group', help='Server group', required=False)

args = parser.parse_args()

#url=args.url

check_url = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        
print('{"error": 0, "status": 0, "progress": 0, "no_progress":0, "message": "Installing tools for monitoring the server..."}')

#check_url.match(args.url) and 

if check_url.match(args.url_stats):
    

    # Create scripts pzoo
    if call("sudo /opt/pythonenv/bin/pip3 install --upgrade git+https://bitbucket.org/paramecio/pastafaristats", shell=True)>0:
        print('Error, cannot install pastafari stats')
        exit(1)
    else:
        print('Added pastafari stats')
        
    # Add configuration to pastafari stats
    
    if not os.path.isdir('/etc/pastafari'):
        # Create pastafari dir
        p=Path('/etc/pastafari')
        p.mkdir(mode=0o755, parents=False, exist_ok=True)
        
    with open('/etc/pastafari/stats.cfg', 'w') as f:
        
        f.write("[DEFAULT]\n\nurl_server="+args.url_stats+"\ngroup="+args.group)
        
    
    with open('/etc/systemd/system/pastafaristats.service', 'w') as f:
                
        #f.write(systemd_unit)
        f.write('# Save it in /etc/systemd/system/pastafaristats.service\n')
        f.write('[Unit]\n')
        f.write('Description=Pastafari Stats\n')
        f.write('After=syslog.target\n')
        f.write('After=network.target\n\n')

        f.write('[Service]\n')
        f.write('Type=simple\n')
        f.write('User=pzoo\n'.replace('pzoo', args.user))
        f.write('Group=pzoo\n'.replace('pzoo', args.user))
        f.write('ExecStart=/opt/pythonenv/bin/pastafaristats\n')
        f.write('Restart=always\n')
        f.write('Environment=PYTHONUNBUFFERED=1\n\n')

        f.write('[Install]\n')
        f.write('WantedBy=multi-user.target\n')
        
        
    if call("sudo systemctl enable pastafaristats.service && sudo systemctl start pastafaristats.service", shell=True)>0:
        print('{"error": 1, "status": 1, "progress": 100, "no_progress":0, "message": "Error, cannot start pastafari stats"}')
        exit(1)
    else:
        print('{"error": 0, "status": 0, "progress": 100, "no_progress":0, "message": "Pastafari stats ready"}')    

    print('{"error": 0, "status": 0, "progress": 100, "no_progress":0, "message": "Pastafaristats Tools installed..."}')
    
    sleep(1)

else:
    
    print('{"error": 1, "status": 1, "progress": 100, "no_progress":0, "message": "Error installing the module, not valid url"}')

    
    exit(1)
