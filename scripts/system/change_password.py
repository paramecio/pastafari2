#!/opt/pythonenv/bin/python3 -u

import sys
import argparse
import os
from pastafariutils.unix import add_user, del_user, change_password
from pathlib import Path
from subprocess import call, DEVNULL
import json
import time
import shutil
import pwd
import distro
import subprocess

parser=argparse.ArgumentParser(prog='change_password.py', description='A tool for change passwords')

parser.add_argument('--user', help='The unix user', required=True)

parser.add_argument('--password', help='The password of the unix user')

parser.add_argument('--ssh_pub_key_file', help='The ssh pub key of the unix user')

args=parser.parse_args()

if args.password:

    password_user=args.password
                    
    ret_pass=change_password(args.user, password_user)

    if not ret_pass[0]:
        print('Error, cannot change password for %s' % args.user+"\n"+ret_pass[1]+"\n")
        exit(1)   
    else:
        
        print('Changed password succesfully...\n')
        
        
if args.ssh_pub_key_file:
    
    if call("sudo su - %s -s /bin/bash -c 'mkdir -p ~/.ssh && chmod 700 ~/.ssh && touch ~/.ssh/authorized_keys' && sudo cat %s >> /home/%s/.ssh/authorized_keys && sudo usermod --shell /bin/bash %s" % (args.user, args.ssh_pub_key_file, args.user, args.user), shell=True) > 0:
        print('Error, cannot add ssh pub key to user %s' % args.user )
        exit(1)            
    else:
        print('Added ssh key successfully...\n')
