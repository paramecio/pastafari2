#!/opt/pythonenv/bin/python3 -u

# A script for install pzoo user

import subprocess
import argparse
import re
import os
import shutil
import pwd
from subprocess import call
from pathlib import Path
from time import sleep
import distro

parser = argparse.ArgumentParser(description='A script for install leviathan user')

#parser.add_argument('--url', help='The url where notify updates', required=True)
#parser.add_argument('--url_stats', help='The url where pastafaristats notify the stats', required=True)
parser.add_argument('--user', help='The user for pastafari', required=True)
parser.add_argument('--pub_key', help='The pub key used in pastafari user', required=True)
parser.add_argument('--group', help='Server group', required=False)
parser.add_argument('--path', help='A path used for save internal things for pastafari', required=True)

args = parser.parse_args()

#url=args.url

check_url = re.compile(
        r'^(?:http|ftp)s?://' # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|' #domain...
        r'localhost|' #localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})' # ...or ip
        r'(?::\d+)?' # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        
print('{"error": 0, "status": 0, "progress": 0, "no_progress":0, "message": "Installing tools for monitoring the server..."}')

#check_url.match(args.url) and 

#if check_url.match(args.url_stats):

try:
    u=pwd.getpwnam(args.user)
    
    if call("sudo userdel -r %s" % args.user, shell=True) > 0:
        print('Error, user with same username exists and cannot delete of the server')
        exit(1)
    else:
        print('Cleaning user with the same name')
    
except:
    pass

# Create users

if call("sudo useradd -m -s /bin/bash %s" % args.user, shell=True) > 0:
    
    # Delete user with this name, you should not install it in a old server. 
    
    if call("sudo userdel -r %s" % args.user, shell=True) > 0:
        print('Error, remove the old user')
        exit(1)            
    else:

        if call("sudo useradd -m -s /bin/sh %s" % args.user, shell=True) > 0:
            print('Error, cannot add a new user')
            exit(1)            
    
else:
    print('Added user')
    
if call("sudo mkdir -p /home/"+args.user+"/.ssh && sudo chown "+args.user+":"+args.user+" /home/"+args.user+"/.ssh && sudo chmod 700 /home/"+args.user+"/.ssh", shell=True) > 0:
    print('Error, cannot add ssh directory')
    exit(1)
else:
    print('Added ssh directory')
    
if call("sudo cp "+args.pub_key+" /home/"+args.user+"/.ssh/authorized_keys && sudo chown "+args.user+":"+args.user+" /home/"+args.user+"/.ssh/authorized_keys && sudo chmod 600 /home/"+args.user+"/.ssh/authorized_keys", shell=True) > 0:
    print('Error, cannot pub key to user')
    exit(1)
else:
    print('Added pub key to user')
    
# Edit sudo file

with open('modules/pastafari2/scripts/system/sudoers.d/spanel') as f:
    sudoers=f.read()

with open('/etc/sudoers.d/spanel', 'w') as f:
    sudoers=sudoers.replace("spanel", args.user)
    f.write(sudoers)

# Installing composer things for php alerts...


#mkdir $HOME/pzoo/scripts

#chown $USER:$USER $HOME/pzoo/scripts

#su - $USER -s /bin/bash -c "composer --working-dir=$HOME/scripts require guzzlehttp/guzzle:~6.0"

args.path=os.path.basename(args.path)

pt=Path('/home/'+args.user+'/'+args.path+'/tasks')

pt.mkdir(mode=0o755, parents=True, exist_ok=True)

shutil.chown('/home/'+args.user+'/'+args.path, args.user, args.user)

shutil.chown('/home/'+args.user+'/'+args.path+'/tasks', args.user, args.user)

# Create scripts pzoo
"""
if call("sudo /opt/pythonenv/bin/pip3 install --upgrade git+https://bitbucket.org/paramecio/pastafaristats", shell=True)>0:
    print('Error, cannot install pastafari stats')
    exit(1)
else:
    print('Added pastafari stats')
    
# Add configuration to pastafari stats

if not os.path.isdir('/etc/pastafari'):
    # Create pastafari dir
    p=Path('/etc/pastafari')
    p.mkdir(mode=0o755, parents=False, exist_ok=True)
    
with open('/etc/pastafari/stats.cfg', 'w') as f:
    
    f.write("[DEFAULT]\n\nurl_server="+args.url_stats+"\ngroup="+args.group)
    

with open('/etc/systemd/system/pastafaristats.service', 'w') as f:
            
    #f.write(systemd_unit)
    f.write('# Save it in /etc/systemd/system/pastafaristats.service\n')
    f.write('[Unit]\n')
    f.write('Description=Pastafari Stats\n')
    f.write('After=syslog.target\n')
    f.write('After=network.target\n\n')

    f.write('[Service]\n')
    f.write('Type=simple\n')
    f.write('User=pzoo\n'.replace('pzoo', args.user))
    f.write('Group=pzoo\n'.replace('pzoo', args.user))
    f.write('ExecStart=/opt/pythonenv/bin/pastafaristats\n')
    f.write('Restart=always\n')
    f.write('Environment=PYTHONUNBUFFERED=1\n\n')

    f.write('[Install]\n')
    f.write('WantedBy=multi-user.target\n')
    
    
if call("sudo systemctl enable pastafaristats.service && sudo systemctl start pastafaristats.service", shell=True)>0:
    print('Error, cannot start pastafari stats')
    exit(1)
else:
    print('Pastafari stats ready')    

print('{"error": 0, "status": 0, "progress": 100, "no_progress":0, "message": "Tools installed..."}')
"""

sleep(1)

linux_distro=distro.id()

# sudo su - %s -s /bin/bash -c 'mkdir -p %s'

if linux_distro=='arch':
    print('Add utilities for archlinux distro...')
    if call("sudo su - %s -s /bin/bash -c 'sudo pacman -S --noconfirm --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si  --noconfirm --needed'" % args.user, shell=True) > 0:
        print('Error, cannot add utilities for archlinux')
        exit(1)

"""
else:
    
    print('Error installing the module, not valid url')
    
    exit(1)
"""
