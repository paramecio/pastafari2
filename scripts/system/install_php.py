#!/opt/pythonenv/bin/python3 -u

# A script for install pzoo user

from subprocess import call
from time import sleep
import distro

linux_distro=distro.id()

print('{"error": 0, "status": 0, "progress": 0, "no_progress":0, "message": "Installing php..."}')

sleep(1)

if linux_distro=='arch':

    if call("sudo pacman -S --noconfirm php php-gd php-pgsql php-snmp php-sodium php-sqlite", shell=True) > 0:
        print('Error, cannot install php...')
        exit(1)            
        
elif linux_distro=='debian' or linux_distro=='ubuntu':
    
    if call('sudo DEBIAN_FRONTEND="noninteractive" apt-get install -y php-fpm php-gd php-json php-mysql php-curl php-mbstring php-intl php-imagick php-xml php-zip php-redis unzip', shell=True) > 0:
        print('Error, cannot install php...')
        exit(1)            
elif linux_distro=='rocky' or linux_distro=='fedora' or linux_distro=='almalinux':
    
    if linux_distro=='rocky' or linux_distro=='almalinux':
    
        if call("sudo dnf install -y php php-gd php-mysqlnd php-fpm php-zip", shell=True) > 0:
            print('Error, cannot install php...')
            exit(1)            
    
    if linux_distro=='fedora':
            
        # In fedora, install some extra packages for get modular php packages from remi. 
        
        fedora_version=distro.version()
        
        if call("dnf -y install https://rpms.remirepo.net/fedora/remi-release-%s.rpm && dnf config-manager --set-enabled remi" % fedora_version, shell=True) > 0:
            print('Error, cannot enable remi repo...')
            exit(1)                     
    
        if call("sudo dnf -y install php82 php82-php-gd php82-php-mysqlnd php82-php-imap php82-php-intl php82-php-fpm php82-php-process composer unzip", shell=True) > 0:
            print('Error, cannot install php...')
            exit(1)            
