#!/usr/bin/python3 -u

import sys
import argparse
import os
from pastafariutils.unix import add_user, del_user, change_password
from pathlib import Path
from subprocess import call, DEVNULL
import json
import time
import shutil
import pwd
import distro
import subprocess

parser=argparse.ArgumentParser(prog='change_password_user.py', description='A tool for change passwords')

parser.add_argument('--password', help='The password of the unix user', required=True)

args=parser.parse_args()

