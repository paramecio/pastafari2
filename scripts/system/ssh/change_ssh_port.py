#!/opt/pythonenv/bin/python3 -u

import sys
import subprocess
import argparse
import platform
import distro
import os

pyv=platform.python_version_tuple()

if pyv[0]!='3':
	print('Need python 3 for execute this script')
	sys.exit(1)

parser = argparse.ArgumentParser(description='Script for ssh port of the server.')

parser.add_argument('--ssh_port', help='The new ssh port', required=True)

args = parser.parse_args()

linux_distro=distro.id()

try:
    
    ssh_port=int(args.ssh_port)
    
except:
    
    print('{"error": 1, "status": 1, "progress": 100, "no_progress":0, "message": "Error: wrong port value"}')
    
    exit(1)

print('Changing ssh port...')

#sudo su -s /bin/bash -c ' echo "Port=%i" > /etc/ssh/sshd_config.d/port.conf' % ssh_port

#if subprocess.call('sudo echo "Port=%i" > /etc/ssh/sshd_config.d/port.conf' % ssh_port,  shell=True) > 0:
if os.path.isfile('/etc/sshd_config.d/port.conf'):
    if subprocess.call("sudo cp /etc/sshd_config.d/port.conf /etc/sshd_config.d/port.conf.bak", shell=True) > 0: 
        print('Error: cannot save the old sshd configuration')
        sys.exit(1)

if subprocess.call("sudo su -s /bin/bash -c ' echo \"Port=%i\" > /etc/ssh/sshd_config.d/port.conf'" % ssh_port, shell=True) > 0: 
        
        #print('{"error": 1, "status": 1, "progress": 100, "no_progress":0, "message": "Error: cannot save the new sshd configuration'+('sudo echo "Port=%i" > /etc/ssh/sshd_config.d/port.conf' % ssh_port)+'"}')
        print('Error: cannot save the new sshd configuration sudo echo "Port=%i" > /etc/ssh/sshd_config.d/port.conf\n' % ssh_port)
        sys.exit(1)

if linux_distro=='fedora' or linux_distro=='centos' or linux_distro=='almalinux' or linux_distro=='rocky':
    if subprocess.call("sudo getenforce", shell=True)==0:
        if subprocess.call("sudo semanage port -a -t ssh_port_t -p tcp %s" % ssh_port, shell=True) > 0: 
            print('WARNING: probably the port is open for ssh in selinux\n')
            #sys.exit(1)

if subprocess.call("sudo systemctl status firewalld", shell=True)==0:
     if subprocess.call("sudo firewall-cmd --add-port=%s/tcp --permanent && sudo firewall-cmd --reload" % ssh_port, shell=True) > 0:
        print('Error, cannot set firewall services for sshd...\n')
        exit(1)            

if subprocess.call("sudo systemctl restart sshd", shell=True) > 0: 
        
        #Fallout to 
        #print('{"error": 1, "status": 1, "progress": 100, "no_progress":0, "message": "Error: cannot save the new sshd configuration'+('sudo echo "Port=%i" > /etc/ssh/sshd_config.d/port.conf' % ssh_port)+'"}')
        
        print('Error: cannot restart new sshd configuration!, dangerous!\n')
        print('Restarting sshd with old value...')
        
        if os.path.isfile('/etc/sshd_config.d/port.conf.bak'):
            if subprocess.call("sudo cp /etc/sshd_config.d/port.conf.bak /etc/sshd_config.d/port.conf", shell=True) > 0: 
                print('Error: cannot get the old sshd configuration for restaure old port!, deleting old configuration and opening port 22!\n')
                sys.exit(1)
                
                if subprocess.call("sudo rm -f /etc/sshd_config.d/port.conf && sudo systemctl restart sshd", shell=True) > 0: 
                    print('Error: cannot delete configuration and restart sshd!!\n')
                    sys.exit(1)
            
        

print('{"error": 0, "status": 0, "progress": 100, "no_progress":0, "message": "Done: changed the ssh port value..."}')
