#!/opt/pythonenv/bin/python3 -u

# A script for install pzoo user

from subprocess import call
from time import sleep
import distro

linux_distro=distro.id()

print('{"error": 0, "status": 0, "progress": 0, "no_progress":0, "message": "Upgrading server..."}')

sleep(1)

if linux_distro=='arch':

    if call("sudo pacman -Syu --noconfirm", shell=True) > 0:
        print('Error, cannot upgrade server...')
        exit(1)            
elif linux_distro=='debian' or linux_distro=='ubuntu':
    
    if call('sudo DEBIAN_FRONTEND="noninteractive" apt-get -y update', shell=True) > 0:
        print('Error, cannot upgrade server...')
        exit(1)            
        
    # Update command
    
    update_command='sudo DEBIAN_FRONTEND="noninteractive" apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade'
    
    if call(update_command, shell=True) > 0:
        print('Error, cannot upgrade server...')
        exit(1)            
elif linux_distro=='rocky' or linux_distro=='fedora' or linux_distro=='alma' or linux_distro=='centos':
    
    if call("sudo dnf upgrade -y", shell=True) > 0:
        print('Error, cannot upgrade server...')
        exit(1)            
        
