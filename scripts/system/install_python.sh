#!/bin/bash

sleep 1

echo '{"error": 0, "status": 0, "progress": 0, "no_progress":0, "message": "Installing Python..."}'

sleep 1

# Get distro version using lsb_release

DISTRO=`( lsb_release -ds || cat /etc/*release || uname -om ) 2>/dev/null | head -n1`

if [[ $DISTRO =~ ^Debian ]] || [[ $DISTRO =~ ^Ubuntu ]]; then
    
    apt-get update
    apt-get install sudo
    sudo apt-get -y install python3 python3-full python3-pip python3-venv libaugeas0
    
elif [[ $DISTRO =~ ^NAME=\"Arch ]]; then

    pacman -Syu --noconfirm sudo

    sudo pacman -S --noconfirm python python-pip sudo augeas python-virtualenv

elif [[ $DISTRO =~ ^NAME=\"Rocky ]] || [[ $DISTRO =~ ^Alma ]];  then

    dnf install -y sudo
    
    sudo dnf upgrade -y

    sudo dnf install -y python python-pip sudo augeas-libs
    #Install EPEL
    dnf config-manager --set-enabled crb
    sudo dnf install -y \
    https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm 
    
    sudo dnf upgrade -y

elif [[ $DISTRO =~ ^Fedora ]];  then

    dnf install -y sudo

    sudo dnf upgrade -y

    sudo dnf install -y python3 python3-pip sudo augeas-libs

else

    echo '{"error": 1, "status": 1, "progress": 100, "no_progress":0, "message": "Error, cannot install python in your distro..."}'
    exit 1;
    
fi

if [ $? -eq 0 ]; then

    #echo "Installed python3 sucessfully if not error..."
    echo '{"error": 0, "status": 0, "progress": 100, "no_progress":0, "message": "Installed python sucessfully..."}'

    sleep 1

else

    echo "Sorry, cannot install python..."

    exit 1;    

fi

echo '{"error": 0, "status": 0, "progress": 0, "no_progress":0, "message": "Installing distro package for python and detect distribution..."}'

# Install virtualenv python and give source in profile.

sudo python3 -m venv /opt/pythonenv/

sudo /opt/pythonenv/bin/pip install --upgrade pip

#echo "export PS_REAL=\$PS1 && source /opt/pythonenv/bin/activate && export PS1=\$PS_REAL && export PYTHONUNBUFFERED=1" >> /etc/profile.d/zpythonenv.sh

#echo "export PYTHONUNBUFFERED=1" >> /etc/profile.d/pythonenv.sh

sleep 1

sudo /opt/pythonenv/bin/pip3 install distro
    
if [ $? -eq 0 ]; then

    #echo "Installed python3 sucessfully if not error..."
    echo '{"error": 0, "status": 0, "progress": 100, "no_progress":0, "message": "Installed distro python package sucessfully..."}'

    sleep 1

else

    echo "Sorry, cannot install distro python package..."

    exit 1;    

fi

sleep 1

# Install certbot for ssl operations with pip (independet of distro)

sudo python3 -m venv /opt/certbot/

sudo /opt/certbot/bin/pip install --upgrade pip

sudo /opt/certbot/bin/pip install certbot certbot

echo "0 0,12 * * * root /opt/certbot/bin/python -c 'import random; import time; time.sleep(random.random() * 3600)' && sudo certbot renew -q" | sudo tee -a /etc/cron.d/certbot > /dev/null

sudo ln -sf /opt/certbot/bin/certbot /usr/bin/certbot

if [ $? -eq 0 ]; then

    #echo "Installed python3 sucessfully if not error..."
    echo '{"error": 0, "status": 0, "progress": 100, "no_progress":0, "message": "Installed certbot package sucessfully..."}'

    sleep 1

else

    echo "Sorry, cannot install certbot python package..."

    exit 1;    

fi
