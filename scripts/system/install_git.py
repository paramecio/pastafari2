#!/opt/pythonenv/bin/python3 -u

# A script for install pzoo user

from subprocess import call
from time import sleep
import distro

linux_distro=distro.id()

print('{"error": 0, "status": 0, "progress": 0, "no_progress":0, "message": "Installing git..."}')

sleep(1)

if linux_distro=='arch':

    # if call("sudo pacman -S --noconfirm --needed git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si", shell=True) > 0:

    if call("sudo pacman -S --noconfirm git", shell=True) > 0:
        print('Error, cannot install git...')
        exit(1)            
elif linux_distro=='debian' or linux_distro=='ubuntu':
    
    if call('sudo DEBIAN_FRONTEND="noninteractive" apt-get install -y git', shell=True) > 0:
        print('Error, cannot install git...')
        exit(1)            
elif linux_distro=='rocky' or linux_distro=='fedora' or linux_distro=='almalinux':
    if call("sudo dnf install -y git", shell=True) > 0:
        print('Error, cannot install git...')
        exit(1)            
