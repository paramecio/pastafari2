#!/opt/pythonenv/bin/python3 -u

import sys
import subprocess
import argparse
import platform
import distro

pyv=platform.python_version_tuple()

if pyv[0]!='3':
	print('Need python 3 for execute this script')
	sys.exit(1)

parser = argparse.ArgumentParser(description='Script for create a new mariadb server.')

parser.add_argument('--password', help='The password of the new server', required=True)
parser.add_argument('--ip', help='The IP where mariadb petitions are listened, if not, only listen in localhost')

args = parser.parse_args()

linux_distro=distro.id()

#Dash, the default debian stretch shell, don't support <<<

#sudo debconf-set-selections <<< 'mariadb-server mariadb-server/root_password password your_password'
#sudo debconf-set-selections <<< 'mariadb-server mariadb-server/root_password_again password your_password'

print('Installing MariaDB...')

if linux_distro=='debian':

    if subprocess.call("sudo DEBIAN_FRONTEND=noninteractive apt-get -y --force-yes install mariadb-server",  shell=True) > 0:
        print('Error, cannot install MariaDB...')
        sys.exit(1)
    
    # sed -i 's/old-text/new-text/g' input.txt
    
    # /etc/mysql/mariadb.conf.d/50-server.cnf
    # bind-address            = 127.0.0.1
    
    print('Setting the password...')
    
    if subprocess.call("sudo mysqladmin -u root password "+args.password,  shell=True) > 0:
        print('Error, cannot set the MariaDB Root password')
        sys.exit(1)
    
    if args.ip:
        if subprocess.call("sudo sed -i 's/bind-address            = 127.0.0.1/bind-address            = {}/g' /etc/mysql/mariadb.conf.d/50-server.cnf".format(args.ip),  shell=True) > 0:
            print('Error, cannot install MariaDB...')
            sys.exit(1)

    print('Setting the password...')
        
elif linux_distro=='arch':
    
    if subprocess.call("sudo pacman -S --noconfirm mariadb expect", shell=True) > 0:
        print('Error, cannot install MariaDB...')
        exit(1)            
    
    if subprocess.call("sudo mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql", shell=True) > 0:
        print('Error, cannot execute mariadb-install-db')
        sys.exit(1)
    
    if subprocess.call("sudo systemctl restart mariadb",  shell=True) > 0:
        print('Error, cannot start mariadb')
        sys.exit(1)
    
    if subprocess.call("sudo MYSQL_PASS=%s ./modules/pastafari2/scripts/servers/databases/mariadb/mysql_secure.sh" % args.password,  shell=True) > 0:
        
        print('Error, cannot set the password')
    
        sys.exit(1)
    
    if args.ip:
        
        print('Changing IP to listen...')
        
        if subprocess.call("sudo sed -i 's/^\[server\]$/[server]\\n\\nbind-address = {}/g' /etc/my.cnf.d/server.cnf".format(args.ip),  shell=True) > 0:
            print('Error, cannot install MariaDB...')
            sys.exit(1)
        
    
    else:
        print('Listen localhost by default...')
    
    
    if subprocess.call("sudo systemctl enable mariadb",  shell=True) > 0:
        print('Error, cannot start mariadb')
        sys.exit(1)

elif linux_distro=='rocky' or linux_distro=='almalinux' or linux_distro=='fedora':
    
    if subprocess.call("sudo dnf install -y mariadb-server expect", shell=True) > 0:
        print('Error, cannot install MariaDB...')
        exit(1)            
        
    if subprocess.call("sudo systemctl restart mariadb",  shell=True) > 0:
        print('Error, cannot start mariadb')
        sys.exit(1)

    print('Setting the password...')
    
    sql_setup="""UPDATE mysql.user SET Password=PASSWORD('{}') WHERE User='root';
    DELETE FROM mysql.user WHERE User=''; 
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1'); 
    DROP DATABASE IF EXISTS test; 
    DELETE FROM mysql.db WHERE Db='test' OR Db='test_\%'; 
    FLUSH PRIVILEGES;""".format(args.password)
    """
    with open('./sql_setup.sql', 'w') as f:
        f.write(sql_setup)
        
    if subprocess.call("sudo mysql --user=root < ./sql_setup.sql",  shell=True) > 0:
        
        print('Error, cannot set the password')
        sys.exit(1)
    else:
        print('Mariadb installed successfully')
        sys.exit(0)
    """
    
    if subprocess.call("sudo MYSQL_PASS=%s ./modules/pastafari2/scripts/servers/databases/mariadb/mysql_secure.sh" % args.password,  shell=True) > 0:
        
        print('Error, cannot set the password')
        sys.exit(1)
            
    if args.ip:
        
        print('Changing IP to listen...')
        if subprocess.call("sudo sed -i 's/pid-file=\/run\/mariadb\/mariadb\.pid/pid-file=\/run\/mariadb\/mariadb.pid\\nbind-address = {}/g' /etc/my.cnf.d/mariadb-server.cnf".format(args.ip),  shell=True) > 0:
            print('Error, cannot install MariaDB...')
            sys.exit(1)
        
        #Open firewall
        #firewall-cmd --add-service=mysql && firewall-cmd --add-service=mysql --permanent
        
        if subprocess.call("sudo firewall-cmd --add-service=mysql && sudo firewall-cmd --add-service=mysql --permanent",  shell=True) > 0:
            print('Error, cannot open mariadb port in firewalld...')
            sys.exit(1)
        
    else:
        print('Listen localhost by default...')
            
    if subprocess.call("sudo systemctl enable mariadb",  shell=True) > 0:
        print('Error, cannot enable mariadb')
        sys.exit(1)
    
    print('Mariadb installed successfully')
    sys.exit(0)

if subprocess.call("sudo systemctl restart mariadb",  shell=True) > 0:
	print('Error, cannot restart mariadb')
	sys.exit(1)

    
"""	
if subprocess.call("sudo echo 'mariadb-server mariadb-server/root_password_again password "+args.password+"' | sudo debconf-set-selections",  shell=True) > 0:
	print('Error, cannot set the password again')
	sys.exit(1)
"""
    
print('Setted the password')
