#!/opt/pythonenv/bin/python3 -u

import sys, os
import subprocess
import argparse
import platform
import shutil
import pathlib
import distro
import pwd
import getpass
import re
import platform

#import pymysql.cursors
#pymysql.install_as_MySQLdb

pyv=platform.python_version_tuple()

if pyv[0]!='3':
	print('Need python 3 for execute this script')
	sys.exit(1)
"""
parser = argparse.ArgumentParser(description='Script for create a Gitea site.')

parser.add_argument('--domain', help='The domain where is the site', required=True)
parser.add_argument('--home_user', help='The name of the new user', required=True)
parser.add_argument('--user', help='The name of the domain user', required=True)
parser.add_argument('--path', help='The path of the gitea install')

args = parser.parse_args()
"""

linux_distro=distro.id()
"""
home_user=args.home_user

if home_user.find('/', -1)==-1:
    print("Error: you need backslash in home_user option")
    exit(1)

user=args.user
"""

apache_cmd='apache2'
apachectl='apache2ctl'

if linux_distro!='debian' and linux_distro!='ubuntu':
    
    apache_cmd='httpd'
    apachectl='apachectl'
    
    if linux_distro=='rocky' or linux_distro=='fedora':
        apache_group='apache'
        
    if linux_distro=='arch':
        apache_group='http'

system_user=getpass.getuser()
"""
path='/'

if args.path:
    
    if args.path.find('/', -1)==-1:
        print("Error: you need backslash in path option")
        exit(1)
    
    path=args.path[:-1]
    
    if path=='':
        path='/'

"""

print("Downloading gitea binary...")

arch_cpu=platform.uname().machine

if arch_cpu!='aarch64' and arch_cpu!='x86_64':
    print("Error, {} is not supported\n".format(arch_cpu))
    sys.exit(1)



# aarch64 x86_64 only 

#sudo wget -O /tmp/gitea https://dl.gitea.io/gitea/1.20/gitea-1.20-linux-amd64 && sudo mv /tmp/gitea /usr/local/bin
#curl -s https://api.github.com/repos/go-gitea/gitea/releases/latest | grep 'browser_download_url' | cut -d\" -f4 | grep 'linux-amd64$'

archs={}

archs['aarch64']='arm64'
archs['x86_64']='amd64'

if os.path.isfile('/usr/local/bin/gitea'):
    print("Error, an installation of gitea exists, please, delete old installation\n")
    sys.exit(1)

#get_url_last_version="wget -O /tmp/gitea `curl -s https://api.github.com/repos/go-gitea/gitea/releases/latest | grep 'browser_download_url' | cut -d '\"' -f4 | grep 'linux-{}$'` && sudo mv /tmp/gitea /usr/local/bin && sudo useradd -m -s /bin/bash gitea && chmod +x /usr/local/bin/gitea".format(archs[arch_cpu])

# curl -o file.pdf https://your-domain-name/long-file-name.pdf
# wget -O /tmp/gitea https://dl.gitea.com/gitea/$GITEA_VERSION/gitea-$GITEA_VERSION-linux-{} &&\

get_url_last_version="curl -s https://api.github.com/repos/go-gitea/gitea/releases/latest | grep 'browser_download_url' | cut -d '\"' -f4 | grep 'linux-{}$' | grep -Eo '[0-9]\.[0-9]+\.[0-9]+' | head -1".format(archs[arch_cpu])

print("Downloading and installing Gitea...\n")

#if subprocess.call(get_url_last_version,  shell=True) > 0:

proc=subprocess.Popen(get_url_last_version, shell=True, stdout=subprocess.PIPE)

if proc.returncode:
    print("Error, cannot download the last version for Gitea\n")
    sys.exit(1)

gitea_version=proc.stdout.read().decode('UTF-8').strip()

download_gitea="wget -O /tmp/gitea https://dl.gitea.com/gitea/{}/gitea-{}-linux-{} &&\
sudo mv /tmp/gitea /usr/local/bin && \
sudo useradd -m -s /bin/bash gitea && chmod +x /usr/local/bin/gitea".format(gitea_version, gitea_version, archs[arch_cpu])

print("Downloading Gitea...\n")

if subprocess.call(download_gitea,  shell=True) > 0:
    print("Error, cannot the last version for Gitea\n")
    sys.exit(1)

print("Creating directory system for Gitea..\n")

dir_gitea="sudo mkdir -p /var/lib/gitea/{custom,data,log} && sudo chown -R gitea:gitea /var/lib/gitea/ && sudo chmod -R 750 /var/lib/gitea/ && sudo mkdir /etc/gitea && sudo chown root:gitea /etc/gitea && sudo chmod 770 /etc/gitea"

if subprocess.call(dir_gitea,  shell=True) > 0:
    print("Error, cannot create the directories for Gitea\n")
    sys.exit(1)

print("Creating the Systemd service for GITEA...")

# && sudo systemctl enable --now gitea

"""
if subprocess.call('sudo wget https://raw.githubusercontent.com/go-gitea/gitea/master/contrib/systemd/gitea.service -P /etc/systemd/system/'),  shell=True) > 0:
    print("Error, cannot create the service for Gitea\n")
    sys.exit(1)
"""

if subprocess.call('sudo cp modules/pastafari2/scripts/servers/cvs/files/gitea.service /etc/systemd/system/ && sudo systemctl start gitea && sudo systemctl enable gitea',  shell=True) > 0:
    print("Error, cannot create and run the service for Gitea\n")
    sys.exit(1)

"""
base_name_file='{}-{}.conf'.format(args.domain, name)
    
name_file='/home/{}/{}-{}.conf'.format(system_user, args.domain, name)

with open(name_file, 'w') as f:
    
    f.write("<Location %s>\n" % path)
    #ProxyPass unix:/home/root/flask_rest/flaskrest.sock|http://127.0.0.1/
    f.write("ProxyPass http://127.0.0.1:3000/\n")
    f.write("ProxyPassReverse http://127.0.0.1:3000/\n")
    f.write("</Location>\n")
    
    if path=='/':
    
        f.write("<Location %s.well-known/acme-challenge/>\n" % path)
        f.write("ProxyPass !\n")
        f.write("</Location>\n")

print('Updating apache configuration for wordpress outside of htdocs...')
if subprocess.call('sudo mv {} /etc/{}/vhosts.d/extra && sudo chown root:root /etc/{}/vhosts.d/extra/{}'.format(name_file, apache_cmd, apache_cmd, base_name_file),  shell=True) > 0:
    print('Error')
    sys.exit(1)

print("Preparing apache for use proxy http...")

if linux_distro=='debian' or linux_distro=='ubuntu':
    if subprocess.call('sudo a2enmod proxy_http', shell=True) > 0:
        print("Error: cannot enable proxy_http in apache in debian/ubuntu distro")
        sys.exit(1)

service=args.domain+'-'+name

if subprocess.call("sudo systemctl enable %s && sudo systemctl start %s  && sudo systemctl restart %s" % (service, service, apache_cmd),  shell=True) > 0:
	print("Error: cannot update and restart apache")
	sys.exit(1)
"""

print("Gitea installed successfully! You can access via :3000 http port")

