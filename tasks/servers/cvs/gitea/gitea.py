#!/opt/pythonenv/python3

from modules.pastafari2.libraries.task import Task
from modules.pastafari2.models.pastafari2 import ServerDbTask, ServerGroup
from modules.pastafari2.libraries.configtask import config_task
from settings import config
from paramecio2.libraries.urls import make_url

try:
    from modules.gitea.models.gitea import GiteaServer
    server_db=True
except:
    server_db=False

class ServerTask(Task):

    def __init__(self, server, conn, remote_user='root', remote_password='', private_key='./ssh/id_rsa', password_key='', remote_path='pastafari2', task_id=0, data={}, port=22):
    
        super().__init__(server, conn, remote_user, remote_password, private_key, password_key, remote_path, task_id, data, port)
        
        self.name_task='Install Gitea in a server'
        
        self.description_task='Install Gitea in a server standalone, without proxy.'
        
        self.codename_task='gitea_server_standalone'
        
        #self.files=[['modules/pastafari2/scripts/system/alive.sh', 0o755]]
        
        self.files=[['modules/pastafari2/scripts/servers/cvs/install_gitea.py', 0o755], ['modules/pastafari2/scripts/servers/cvs/files/gitea.service', 0o644]]
        
        self.commands_to_execute=[['modules/pastafari2/scripts/servers/cvs/install_gitea.py', '']]
        
        self.one_time=True
        
        self.version='1.0'


    def post_task(self):
        
        if server_db:
            dbserver=GiteaServer(self.connection)
            
            dbserver.safe_query()
            
            serverdb=ServerDbTask(self.connection)
            
            arr_server=serverdb.set_conditions('WHERE ip=%s', [self.server]).select_a_row_where()
            
            if arr_server:
                dbserver.insert({'server_id': arr_server['id']})
        
        return True
