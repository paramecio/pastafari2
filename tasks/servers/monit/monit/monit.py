#!/opt/pythonenv/python3

from modules.pastafari2.libraries.task import Task
from modules.pastafari2.models.pastafari2 import ServerDbTask, ServerGroup
from modules.pastafari2.libraries.configtask import config_task
from settings import config
from paramecio2.libraries.urls import make_url

class ServerTask(Task):

    def __init__(self, server, conn, remote_user='root', remote_password='', private_key='./ssh/id_rsa', password_key='', remote_path='pastafari2', task_id=0, data={}, port=22):
    
        super().__init__(server, conn, remote_user, remote_password, private_key, password_key, remote_path, task_id, data, port)
        
        #self.files=[['modules/pastafari2/scripts/system/alive.sh', 0o755]]
        
        self.files=[['modules/pastafari2/scripts/system/install_monit_stats.py', 0o755]]
        
        self.commands_to_execute=[]
        
        self.one_time=True
        
        self.version='1.0'
        

    def pre_task(self):
        
        ssh_user=config_task.remote_user
        
        url_stats=config.domain_url+make_url('get_data/'+config.monit_api_key)
        #url_for('monit_app.monit_get_data', api_key=config.monit_api_key)
        
        with self.connection.query('select * from serverdbtask WHERE ip=%s', [self.server]) as cursor:
            arr_server=cursor.fetchone()
        
        if arr_server:
            self.data['group_id']=arr_server['group_id']
            
        if self.data['group_id']!='':
            server_group=ServerGroup(self.connection)
            arr_group=server_group.set_conditions('WHERE id=%s', [self.data['group_id']]).select_a_row_where()
            self.data['group_name']=arr_group['code_group']
        
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/install_monit_stats.py', '--user='+ssh_user+' --url_stats='+url_stats+' --group="'+self.data['group_name']+'"', 'sudo'])
        
        return True
 
        
    """
    def post_task(self):
        
        server=ServerDbTask(self.connection)
        
        server.safe_query()
        
        #'subdomain_id': self.data['subdomain_id']
        
        server.insert({'hostname': self.data['hostname'], 'ip': self.data['ip'], 'group_id': self.data['group_id']})
        
        return True
    """
