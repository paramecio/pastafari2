#/usr/bin/env python3

from modules.pastafari2.libraries.task import Task
#from modules.pastafari.models.tasks import TaskModel
from paramecio2.libraries.db import coreforms
from paramecio2.libraries.db.extrafields.ipfield import IpField
from paramecio2.libraries.formsutils import show_form
from collections import OrderedDict
from modules.pastafari2.models.pastafari2 import ServerDbTask
try:
    from modules.mariadb.models.mariadb import DbServerMariaDb
    server_db=True
except:
    server_db=False
    

class ServerTask(Task):
    
    def __init__(self, server, conn, remote_user='root', remote_password='', private_key='./ssh/id_rsa', password_key='', remote_path='pastafari2', task_id=0, data={}, port=22):
    
        super().__init__(server, conn, remote_user, remote_password, private_key, password_key, remote_path, task_id, data, port)
        
        self.name_task='MariaDB installation'
        
        self.description_task='Installation of a standalone mysql server'
        
        self.codename_task='standalone_mysql'
        
        self.files=[['modules/pastafari2/scripts/servers/databases/mariadb/install_mariadb.py', 0o700], ['modules/pastafari2/scripts/servers/databases/mariadb/mysql_secure.sh', 0o700]]
        
        # Format first array element is command with the interpreter, the task is agnostic, the files in os directory. The commands are setted with 750 permission.
        # First element is the file, next elements are the arguments
        
        #self.commands_to_execute=[['modules/pastafari/scripts/servers/databases/mysql/install_mariadb.py', '']];
        
        #THe files to delete
        
        self.delete_files=[]
        
        self.delete_directories=['modules/pastafari2/scripts/servers/databases/mariadb']
        
        #self.task=Task(conn)
        
        self.one_time=True
        
        self.version='1.0'
        
        self.arr_form=OrderedDict()
        
        self.arr_form['mysql_password']=coreforms.PasswordForm('mysql_password', '')
        
        self.arr_form['mysql_password'].required=True
        
        self.arr_form['mysql_password'].label='The MySQL password used by all servers'
        
        self.arr_form['repeat_mysql_password']=coreforms.PasswordForm('repeat_mysql_password', '')
        
        self.arr_form['repeat_mysql_password'].required=True
        
        self.arr_form['repeat_mysql_password'].label='Repeat MySQL password'
        
        self.arr_form['access_localhost']=coreforms.SimpleTextForm('access_localhost', '')
        
        self.arr_form['access_localhost'].required=True
        
        #self.arr_form['access_localhost'].default_value='1'
        
        self.arr_form['access_localhost'].label='IP for access to mariadb server'
        self.arr_form['access_localhost'].help='Empty if you want only localhost access. If you want all ips, use 0.0.0.0 or if you want other ip, put it'
        
        
        #self.commands_to_execute=[['modules/pastafari/scripts/servers/databases/mariadb/install_mariadb.py', '--password=%s' % self.data['mysql_password']]]

    def post_task(self):
        
        if server_db:
            dbserver=DbServerMariaDb(self.connection)
            
            dbserver.safe_query()
            
            serverdb=ServerDbTask(self.connection)
            
            arr_server=serverdb.set_conditions('WHERE ip=%s', [self.server]).select_a_row_where()
            
            if arr_server:
                dbserver.insert({'server_id': arr_server['id'], 'access_ip': self.data['ip']})
        
        return True

    
    def pre_task(self):
        
        ip_option=''
        
        if 'ip' in self.data:
            ip_option='--ip='+self.data['ip']
        
        self.commands_to_execute=[['modules/pastafari2/scripts/servers/databases/mariadb/install_mariadb.py', '--password=%s %s' % (self.data['mysql_password'], ip_option)]]
        
        return True
    
    def form(self, t, yes_error=False, pass_values=False, values={}):
        
        #Here load the form for it task
        
        return '<h2>Mariadb/MySQL configuration</h2>'+show_form(values, self.arr_form, t, yes_error, pass_values)
    
    def check_form(self, post):
        
        error=False
        
        if 'mysql_password' in post and 'repeat_mysql_password' in post:
            
            if post['mysql_password'].strip()!='' and post['mysql_password']==post['repeat_mysql_password']:
                
                self.data['mysql_password']=post['mysql_password'].strip()
                
            else:
                
                self.arr_form['mysql_password'].error=True
                self.arr_form['mysql_password'].txt_error='Passwords doesn\'t match'
                error=True
        
        if 'access_localhost' in post:
            
            self.data['ip']=''
            
            ip_check=IpField('ip')
            
            ip_host=post['access_localhost'].strip()
            
            if ip_host!='':
            
                ip_host=ip_check.check(ip_host)
                
                if ip_host!='':
                    self.data['ip']=ip_host
                    
                if ip_check.error:
                    
                    self.arr_form['access_localhost'].error=True
                    self.arr_form['access_localhost'].txt_error='Wrong ip format'
                    error=True
                
            
        if error:
            return False
        else:
            return True
        
        
