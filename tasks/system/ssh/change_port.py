#/usr/bin/env python3

from modules.pastafari2.libraries.task import Task
#from modules.pastafari.models.tasks import TaskModel
from paramecio2.libraries.db import coreforms
from paramecio2.libraries.db.extrafields.ipfield import IpField
from paramecio2.libraries.formsutils import show_form
from collections import OrderedDict
from modules.pastafari2.models.pastafari2 import ServerDbTask
    

class ServerTask(Task):
    
    def __init__(self, server, conn, remote_user='root', remote_password='', private_key='./ssh/id_rsa', password_key='', remote_path='pastafari2', task_id=0, data={}, port=22):
    
        super().__init__(server, conn, remote_user, remote_password, private_key, password_key, remote_path, task_id, data, port)
        
        self.name_task='Change port in SSH'
        
        self.description_task='This task change the port used for ssh access'
        
        self.codename_task='change_ssh_port'
        
        self.files=[['modules/pastafari2/scripts/system/ssh/change_ssh_port.py', 0o700]]
        
        # Format first array element is command with the interpreter, the task is agnostic, the files in os directory. The commands are setted with 750 permission.
        # First element is the file, next elements are the arguments
        
        #self.commands_to_execute=[['modules/pastafari/scripts/servers/databases/mysql/install_mariadb.py', '']];
        
        #THe files to delete
        
        self.delete_files=[]
        
        self.delete_directories=['modules/pastafari2/scripts/system/ssh']
        
        #self.task=Task(conn)
        
        self.one_time=False
        
        self.version='1.0'
        
        self.arr_form=OrderedDict()
        
        self.arr_form['ssh_port']=coreforms.BaseForm('ssh_port', '')
        
        self.arr_form['ssh_port'].required=True
        
        self.arr_form['ssh_port'].label='SSH Port for the server'
        self.arr_form['ssh_port'].help='Change the ssh port to the servers selected to execute this task'
        
        
        #self.commands_to_execute=[['modules/pastafari/scripts/servers/databases/mariadb/install_mariadb.py', '--password=%s' % self.data['mysql_password']]]

    def post_task(self):
        
        serverdb=ServerDbTask(self.connection)
        
        arr_server=serverdb.set_conditions('WHERE ip=%s', [self.server]).select_a_row_where()
        
        if arr_server:
            #dbserver.insert({'server_id': arr_server['id'], 'access_ip': self.data['ip']})
            serverdb.query('update serverdbtask set ssh_port=%s WHERE id=%s', [self.data['ssh_port'], arr_server['id']])
        
        return True

    
    def pre_task(self):

        self.commands_to_execute=[['modules/pastafari2/scripts/system/ssh/change_ssh_port.py', '--ssh_port=%s' % (self.data['ssh_port'])]]
        
        return True
    
    def form(self, t, yes_error=False, pass_values=False, values={}):
        
        #Here load the form for it task
        
        return '<h2>Change SSH port</h2>'+show_form(values, self.arr_form, t, yes_error, pass_values)
    
    def check_form(self, post):
        
        error=False
        
        ssh_port=0
        
        if 'ssh_port' in post:
            try:
                ssh_port=int(post['ssh_port'])
                
                if ssh_port<10000:
                    self.arr_form['ssh_port'].error=True
                    self.arr_form['ssh_port'].txt_error='Error: port need more than 10000'
                    error=True
                
            except:
                
                self.arr_form['ssh_port'].error=True
                self.arr_form['ssh_port'].txt_error='Error: not valid ssh port'
                error=True
        
                
            
        if error:
            return False
        else:
            
            self.data['ssh_port']=str(ssh_port)
            
            return True
        
        
