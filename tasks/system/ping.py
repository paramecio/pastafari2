from modules.pastafari2.libraries.task import Task
from modules.pastafari2.models.pastafari2 import ServerDbTask

class ServerTask(Task):

    def __init__(self, server, conn, remote_user='root', remote_password='', private_key='./ssh/id_rsa', password_key='', remote_path='leviathan', task_id=0, data={}, port=22):
    
        super().__init__(server, conn, remote_user, remote_password, private_key, password_key, remote_path, task_id, data, port)
        
        self.files=[['modules/pastafari2/scripts/system/alive.sh', 0o755]]
        
        self.commands_to_execute=[]
        
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/alive.sh', ''])
