from modules.pastafari2.libraries.task import Task
from modules.pastafari2.models.pastafari2 import ServerDbTask

class ServerTask(Task):

    def __init__(self, server, conn, remote_user='root', remote_password='', private_key='./ssh/id_rsa', password_key='', remote_path='pastafari2', task_id=0, data={}, port=22):
    
        super().__init__(server, conn, remote_user, remote_password, private_key, password_key, remote_path, task_id, data, port)
        
        #self.files=[['modules/pastafari2/scripts/system/alive.sh', 0o755]]
        
        self.files=[['modules/pastafari2/scripts/system/install_pzoo_stats.py', 0o755]]
        self.files.append(['modules/pastafari2/scripts/system/install_python.sh', 0o755])
        self.files.append(['modules/pastafari2/scripts/system/install_psutil.sh', 0o755])
        self.files.append(['modules/pastafari2/scripts/system/install_git.py', 0o755])
        self.files.append(['modules/pastafari2/scripts/system/install_php.py', 0o755])
        self.files.append(['modules/pastafari2/scripts/system/install_unixutils.sh', 0o755])
        self.files.append(['modules/pastafari2/scripts/system/sudoers.d/spanel', 0o640])
        
        self.files.append([self.data['pub_key'], 0o600])
        
        self.commands_to_execute=[]
        
        #self.commands_to_execute.append(['modules/pastafari2/scripts/system/alive.sh', ''])
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/install_python.sh', ''])
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/install_git.py', ''])
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/install_php.py', ''])
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/install_unixutils.sh', ''])
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/install_psutil.sh', ''])
        
        #' --url_stats='+self.data['url_stats']+
        
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/install_pzoo_stats.py', '--user='+self.data['ssh_user']+' --pub_key='+self.data['pub_key']+' --group="'+self.data['group_name']+'" --path='+remote_path])

    def post_task(self):
        
        server=ServerDbTask(self.connection)
        
        server.safe_query()
        
        #'subdomain_id': self.data['subdomain_id']
        
        server.insert({'hostname': self.data['hostname'], 'ip': self.data['ip'], 'group_id': self.data['group_id'], 'ssh_port': self.port})
        
        return True
