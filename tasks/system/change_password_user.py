from modules.pastafari2.libraries.task import Task
from modules.pastafari2.models.pastafari2 import ServerDbTask
import os
from subprocess import call

class ServerTask(Task):

    def __init__(self, server, conn, remote_user='root', remote_password='', private_key='./ssh/id_rsa', password_key='', remote_path='pastafari2', task_id=0, data={}, port=22):
    
        super().__init__(server, conn, remote_user, remote_password, private_key, password_key, remote_path, task_id, data, port)
        
        #self.files=[['modules/pastafari2/scripts/system/alive.sh', 0o755]]
        
        self.files=[['modules/pastafari2/scripts/system/change_password.py', 0o755]]
        
        #self.files.append([self.data['ssh_pub_key'], 0o600])
        
        self.commands_to_execute=[]
        
        #self.commands_to_execute.append(['modules/pastafari2/scripts/system/alive.sh', ''])
        
        #self.commands_to_execute.append(['modules/pastafari2/scripts/system/install_pzoo_stats.py', '--user='+self.data['ssh_user']+' --pub_key='+self.data['pub_key']+' --url_stats='+self.data['url_stats']+' --group="'+self.data['group_name']+'" --path='+remote_path])
        
    def pre_task(self):
        
        ssh_pub_key_file=''
        
        final_ssh_pub=''
        
        if self.data['ssh_pub_key']!='':
            with open('./modules/webservers/scripts/files/id_rsa.pub', 'w') as f:
                f.write(self.data['ssh_pub_key'])
                ssh_pub_key_file='./modules/webservers/scripts/files/id_rsa.pub'
                self.files.append([ssh_pub_key_file, 0o600])
                
            # ssh-keygen -l -f id_rsa.pub
            
            if call("ssh-keygen -l -f {}".format(ssh_pub_key_file),  shell=True) > 0:
                self.logtask.insert({'status':1, 'progress': 100, 'error': 1, 'task_id': self.id, 'server': self.server, 'message': 'INVALID PUB KEY %s' % "{}".format(ssh_pub_key_file)})
                #os.unlink(ssh_pub_key_file)
                return False
                    
        else:
            self.logtask.insert({'status':1, 'progress': 100, 'error': 1, 'task_id': self.id, 'server': self.server, 'message': 'No exists ssh pub key'})
            return False
        
        final_ssh_pub='--ssh_pub_key_file="'+ssh_pub_key_file+'"'
        """
        final_password=''
        
        if self.data['password']!='':
            final_password='--password=%s' % self.data['password']
        """
        
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/change_password.py', '--user=%s %s' % (self.data['user'], final_ssh_pub), 'sudo'])
        
        return True

    
    def post_task(self):
        
        ssh_pub_key_file='./modules/webservers/scripts/files/id_rsa.pub'
        
        if os.path.isfile(ssh_pub_key_file):
            os.unlink(ssh_pub_key_file)
            
        return True
    
