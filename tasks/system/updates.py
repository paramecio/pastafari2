from modules.pastafari2.libraries.task import Task
from modules.pastafari2.models.pastafari2 import ServerDbTask, UpdateServerScripts

class ServerTask(Task):

    def __init__(self, server, conn, remote_user='root', remote_password='', private_key='./ssh/id_rsa', password_key='', remote_path='pastafari2', task_id=0, data={}, port=22):
    
        super().__init__(server, conn, remote_user, remote_password, private_key, password_key, remote_path, task_id, data, port)
        
        self.files=[['modules/pastafari2/scripts/system/updates.py', 0o755]]
        
        self.commands_to_execute=[]
        
        self.commands_to_execute.append(['modules/pastafari2/scripts/system/updates.py', ''])

        serverdb=ServerDbTask(conn)

        arr_server=serverdb.set_conditions('WHERE ip=%s', [server]).select_a_row_where()
        
        if arr_server:
        
            update_server=UpdateServerScripts(conn)
            
            arr_update=update_server.set_conditions('WHERE server_id=%s', [arr_server['id']]).select_to_array()
            
            for upd in arr_update:
                
                self.files.append([upd['file'], 0o755])
                self.commands_to_execute.append([upd['file'], upd['args']])
